package hw3;

import java.util.Random;
import java.util.Scanner;

public class Ship_battle {

    static void print(char set[][]) {
        for (int i = 0; i < 6; i++) System.out.print(i + " ");
        System.out.println();
        for (int i = 1; i < 6; i++) {
            System.out.print(i + "|");
            for (int j = 0; j < 5; j++) {
                System.out.print(set[i][j] + "|");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        /// declaration
        int row, col;
        Scanner sc = new Scanner(System.in);
        char set[][] = new char[6][6];
        Random randomGenerator = new Random();
        int a = randomGenerator.nextInt(5) + 1;
        int b = randomGenerator.nextInt(5) + 1;
//// set table with -
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 5; j++) {
                set[i][j] = '-';
            }
        }
//////// starting

        System.out.println("All set. Get ready to rumble!");
        print(set);
        //// gets values
        row = sc.nextInt();
        col = sc.nextInt() - 1;

        //// game start
        while (true) {
            if (row != a && col != b) {
                set[row][col] = '*';
                print(set);
                row = sc.nextInt();
                col = sc.nextInt() - 1;
            } else {
                set[row][col] = 'x';
                print(set);
                System.out.println("You Won");
                break;
            }


        }
    }

}
