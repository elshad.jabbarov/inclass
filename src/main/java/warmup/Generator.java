package warmup;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Generator {
    public static void main(String[] args) {
      Map  <Integer, List<Integer>> collect = Stream.generate(() -> Math.random() * 40 - 20)
                .limit(10)
                .map(Double::intValue)
                .collect(Collectors.groupingBy( i->i % 3)) ;

        System.out.println(collect);
    }
}
