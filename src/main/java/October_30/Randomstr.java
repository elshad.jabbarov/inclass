package October_30;

import java.util.Random;

public class Randomstr {
    public String Randomstr() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZqwertyyuiiplkjhgfdsazxcvbnm";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        int k =30-(int) (Math.random()*20);
        while (salt.length() < k) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
}
