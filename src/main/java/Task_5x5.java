public class Task_5x5 {
    public static void main(String[] args) {
        int [] [] a = new int  [5][5];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                a[i][j] = (int) (Math.random()*10-5);
            }
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
             if(a[i][j]<0)   System.out.print("**");
             else   System.out.print(a[i][j]);
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                if(a[i][j]>0)   System.out.print(" * ");
                else   System.out.print(a[i][j]);
            }
            System.out.println();
        }

    }
}
